//Globals
var claimedDeviceUUID = ''

var conn = skynet.createConnection({
	"protocol": "mqtt",
	"qos": 1
});

conn.on('notReady', function(data){
	console.log('UUID FAILED AUTHENTICATION!');
	console.log(data);
});

conn.on('ready', function(data){
	console.log('UUID AUTHENTICATED!')

	// Receive an array of device UUIDs based on user defined search criteria
	conn.devices({
		"type"		: "ESSU1415-G12-car"
	}, function (data) {
		for( var carKey in data.devices ) {
			if(data.devices[carKey].online) displayCar(data.devices[carKey], ".car-devices")
		}
	});
});

function claimDevice(uuid) {
	conn.claimdevice({"uuid": uuid, "token" : "ESSU1415" }, function (data) {
		console.log('Claimed Device:')
		console.log(data);

		claimedDeviceUUID = uuid
		//sendCommand(uuid, 'fwd')
	});
}

function sendCommand(device, command) {
	conn.message({
		"devices": device,
		"payload": {
			"command": command
		}
	});
}

function moveFWD() {
	sendCommand(claimedDeviceUUID, 'fwd')
}

function moveRVS() {
	sendCommand(claimedDeviceUUID, 'rvs')
}

function moveLFT() {
	sendCommand(claimedDeviceUUID, 'lft')
}

function moveRGT() {
	sendCommand(claimedDeviceUUID, 'rgt')
}

function moveSTP() {
	sendCommand(claimedDeviceUUID, 'stp')
}

function laserLFT() {
	sendCommand(claimedDeviceUUID, 'serLft')
}

function laserRGT() {
	sendCommand(claimedDeviceUUID, 'serRgt')
}

function fireLaser() {
	sendCommand(claimedDeviceUUID, 'fir')
}
