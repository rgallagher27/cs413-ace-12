

function displayCar(car, div)
{
	console.log('Car data:')
	console.log(car)

	$( div ).append('<li class="col-md-4"><p>'
	+ car.uuid
	+ '</p>'
	+ '<input type="button" value="Choose" class="car-button" id="'
	+ car.uuid
	+ '"></li>');

	$( '.car-button' ).click(function(e) {
		claimDevice(e.target.id)
	});

	document.onkeypress = function (e) {
		e = e || window.event;
		
		switch (e.keyCode) {
			case 119: //forward - W
				sendCommand(claimedDeviceUUID, 'fwd')
				break;

			case 97: //Left - A
				sendCommand(claimedDeviceUUID, 'lft')
				break;

			case 100: //Right - D
				sendCommand(claimedDeviceUUID, 'rgt')
			break;

			case 115: //Back - S
				sendCommand(claimedDeviceUUID, 'rvs')
			break;

			case 107: //Laser Left - K
				sendCommand(claimedDeviceUUID, 'serLft')
			break;

			case 108: //Laser Right - l
				sendCommand(claimedDeviceUUID, 'serRgt')
			break;

			case 102: //Fire - F
				sendCommand(claimedDeviceUUID, 'fir')
			break;

			case 102: //Stop - SpaceBar
				sendCommand(claimedDeviceUUID, 'stp')
			break;

			default:
		}
	};

}
