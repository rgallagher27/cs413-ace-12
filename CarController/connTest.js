var meshbluConn = require("../CommonSource/meshbluConnect")

meshbluConn
.connect("car", "hello-world")
.then(function success(connection) {

	connection.on('message', function(message){
		console.log('message received', message.payload.command)

	})

}, function failure(err) {
	console.log("Error connecting to meshblu")
	console.log(err)
})
