var meshbluConn = require("../CommonSource/meshbluConnect")

var five 	= require("johnny-five")
var board = new five.Board()

console.log("Car controller started")

board.on("ready", function() {

	var config = five.Motor.SHIELD_CONFIGS.ADAFRUIT_V1

	var RightMotor 	= new five.Motor(config.M2)
	var LeftMotor 	= new five.Motor(config.M1)
	var LaserLED 		= new five.Led(13)
	var LaserServo 	= new five.Servo({
		pin: "5",
		type: "standard",
		range: [0,180],
		center: true
	}).stop()

	meshbluConn
	.connect("car", "hello-world")
	.then(function success(connection) {

		connection.on('message', function(message){
			console.log('message received', message.payload.command)

			switch (message.payload.command) {
				case 'fwd':
					stopMotors()
					motorsFWD()
					break;

				case 'rvs':
					stopMotors()
					motorsRVS()
					break;

				case 'lft':
					stopMotors()
					motorsLFT()
					break;

				case 'rgt':
					stopMotors()
					motorsRGT()
					break;

				case 'stp':
					stopMotors()
					break;

				case 'serLft':
					servoLFT()
					break;

				case 'serRgt':
					servoRGT()
					break;

				case 'fir':
					fireLaser()
					break;

				default:
					stopMotors()
			}
		})

	}, function failure(err) {
		console.log("Error connecting to meshblu")
		console.log(err)
	})

	function motorsFWD() {
		LeftMotor.forward(200)
		RightMotor.forward(200)
	}

	function motorsRVS() {
		LeftMotor.reverse(200)
		RightMotor.reverse(200)
	}

	function motorsLFT() {
		LeftMotor.reverse(250)
		RightMotor.forward(250)
	}

	function motorsRGT() {
		LeftMotor.forward(250)
		RightMotor.reverse(250)
	}

	function stopMotors() {
		LeftMotor.stop()
		RightMotor.stop()
	}

	function servoLFT() {
		console.log('should servo left')
		LaserServo.step( -10 )
	}

	function servoRGT() {
		console.log('should servo right')
		LaserServo.step( 10 )
	}

	function fireLaser() {
		LaserLED.on()

		setTimeout(function () {
			LaserLED.off()
			console.log('laser off')
		}, 1000)
	}
})
