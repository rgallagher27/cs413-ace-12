var five 	= require("johnny-five")
var board = new five.Board()

var keypress 	= require('keypress')
		keypress(process.stdin)

var ServoAngle = 90

console.log("Motor Test Started")

board.on("ready", function() {

	var config = five.Motor.SHIELD_CONFIGS.ADAFRUIT_V1

	var RightMotor 	= new five.Motor(config.M2)
	var LeftMotor 	= new five.Motor(config.M1)

/**
	var LaserServo 	= new five.Servo({
		pin: "8",
		type: "continuous"
	}).stop()
**/

  board.repl.inject({
    RightMotor	: RightMotor,
		LeftMotor		: LeftMotor
		//LaserServo	: LaserServo
  })

	process.stdin.on('keypress', function (ch, key) {
		console.log('got "keypress"', key);
		if (key.name == 'w') {
			stop()
			LeftMotor.forward(150)
			RightMotor.forward(150)
		}

		if (key.name == 'a') {
			stop()
			LeftMotor.reverse(150)
			RightMotor.forward(250)
		}

		if (key.name == 'd') {
			stop()
			LeftMotor.forward(250)
			RightMotor.reverse(150)
		}

		if(key.name == 's') {
			stop()
			LeftMotor.reverse(150)
			RightMotor.reverse(150)
		}

		if(key.name == 'space') {
			stop()
		}
/**
		if (key.name == 'up') {
			ServoAngle = 90
			LaserServo.to(ServoAngle)
		}

		if (key.name == 'left') {
			ServoAngle -= 20
			LaserServo.to(ServoAngle)
		}

		if (key.name == 'right') {
			ServoAngle += 20
			LaserServo.to(ServoAngle)
		}
**/
		if(key.name == 'e') {
			process.exit(0)
		}
	})

	function stop() {
		LeftMotor.stop()
		RightMotor.stop()
	}
})
