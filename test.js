var skynet 		= require('skynet')
var keypress 	= require('keypress')

		keypress(process.stdin)

var conn = skynet.createConnection({
	"uuid": "1ddf0c11-4e4e-11e4-9949-7b6f78c51158",
	"token": "000zc52hz5x6flxrbbiaqlcpw240lik9",
	"protocol": "mqtt", // or "websocket"
	"qos": 0, // MQTT Quality of Service (0=no confirmation, 1=confirmation, 2=N/A)
	"server": "http://rgmeshblu.herokuapp.com" // optional - defaults to ws://meshblu.octoblu.com
})

conn.on('notReady', function(data){
	console.log('UUID FAILED AUTHENTICATION!')
	console.log(data)
})

conn.on('ready', function(data){
	console.log('UUID AUTHENTICATED!')

	conn.devices({
    "type":"pi"
  }, function (data) {
    console.log(data);
  })

	process.stdin.on('keypress', function (ch, key) {
	  console.log('got "keypress"', key);
	  if (key && key.name == 'w') {
			conn.message({
				"devices": "bf59eb61-4e57-11e4-9949-7b6f78c51158",
				"payload": {
					"angle"	: 160,
					"motor"	: 10
				},
				"qos": 0
			})
	  } else if(key && key.name == 'p') {
			conn.message({
				"devices": "bf59eb61-4e57-11e4-9949-7b6f78c51158",
				"payload": {
					"angle"	: 90,
					"motor"	: 0
				},
				"qos": 0
			})
		}else if(key && key.name == 'l') {
			process.exit(0)
		}
	})
})

process.stdin.setRawMode(true)
process.stdin.resume()
