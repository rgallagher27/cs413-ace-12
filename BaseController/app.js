var meshbluConn = require("../CommonSource/meshbluConnect")


console.log("Base controller started")


meshbluConn
    .connect("car", "hello-world")
    .then(function success(connection) {

        connection.on('message', function(message){
            console.log('message received', message)
        })

    }, function failure(err) {
        console.log("Error connecting to meshblu")
        console.log(err)
    })