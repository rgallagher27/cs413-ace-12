
switch (process.argv[2]) {
	case "car":
		require("./CarController/app")
		break;

	case "base":
		require("./BaseController/app")
		break;

	default:
		console.log("No valid module selected")
		process.exit(0)
}
