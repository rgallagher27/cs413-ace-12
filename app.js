var skynet 	= require('skynet')


var conn = skynet.createConnection({
	"protocol": "mqtt", // or "websocket"
	"qos": 1, // MQTT Quality of Service (0=no confirmation, 1=confirmation, 2=N/A)
	//"server": "http://rgmeshblu.herokuapp.com" // optional - defaults to ws://meshblu.octoblu.com
})

conn.on('notReady', function(data){
	console.log('UUID FAILED AUTHENTICATION!')
	console.log(data)

	console.log('UUID NOT AUTHENTICATED!')
	// Register a device
	conn.register({
		"type": module
	}, function (data) {
		console.log(data)

		// Login to SkyNet to fire onready event
		conn.authenticate({
			"uuid"	: data.uuid,
			"token"	: data.token
		}, function (data) {
			console.log(data)
		})
	})
})

conn.on('ready', function(data){
	console.log('UUID AUTHENTICATED!')
	console.log(data)

	// Receive an array of device UUIDs based on user defined search criteria
	conn.devices({
		"type":"SUESY1415G12-car"
	}, function (data) {
		console.log(data);

		conn.claimdevice({"uuid": data.devices[0].uuid, "token" : "hello-world" }, function (data) {
			console.log(data);

			conn.mydevices({}, function (data) {
				console.log(data);

				conn.message({
					"devices": data.devices[0].uuid,
					"payload": {
						"hello":"world"
					}
				});
			});

		});

	});
})
