var config 	= require("../Configs/global")

var skynet 	= require('skynet')
var when 		= require('when')

exports.connect = function (module, token)
{
	var deferred 	= when.defer()

	// Create connection to the MeshBlu IoT Network
	var conn = skynet.createConnection({
		"token" 		: token,
		"protocol"	: "mqtt",
		"qos"				: 1
	})

	//If error connecting - Reject
	conn.on('notReady', function(data){
		console.log('UUID NOT AUTHENTICATED!')
		// Register a device
		conn.register({
			"type"		: config.network_id + module,
			"token" 	: token
		}, function (data) {
			console.log('Device reg')
			console.log(data)

			// Login to SkyNet to fire onready event
			conn.authenticate({
				"uuid"	: data.uuid,
				"token"	: data.token
			}, function (data) {
				console.log('Device auth')
				console.log(data)
			})
		})
	})

	//If successful connection - Resolve conn
	conn.on('ready', function(data){
		console.log('UUID AUTHENTICATED!')
		console.log(data)

		deferred.resolve(conn)
	})

	return deferred.promise
}
